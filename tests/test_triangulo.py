#!/usr/bin/python3
# -*- coding: utf-8 -*-

import contextlib
from io import StringIO
import os
import sys
import unittest

from unittest.mock import patch

import triangulo

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')

result_1 = """1
"""

result_2 = """1
22
"""

result_3 = """1
22
333
"""

result_9 = """1
22
333
4444
55555
666666
7777777
88888888
999999999
"""


class TestLine(unittest.TestCase):

    def test_1(self):
        text = triangulo.line(1)
        self.assertEqual(text, "1")

    def test_4(self):
        text = triangulo.line(4)
        self.assertEqual(text, "4444")

    def test_9(self):
        text = triangulo.line(9)
        self.assertEqual(text, "999999999")


class TestTriangle(unittest.TestCase):

    def test_1(self):
        text = triangulo.triangle(1)
        self.assertEqual(text, result_1)

    def test_2(self):
        text = triangulo.triangle(2)
        self.assertEqual(text, result_2)

    def test_3(self):
        text = triangulo.triangle(3)
        self.assertEqual(text, result_3)

    def test_9(self):
        text = triangulo.triangle(9)
        self.assertEqual(text, result_9)

    def test_10(self):
        with self.assertRaises(ValueError):
            text = triangulo.triangle(10)


class TestPrograma(unittest.TestCase):

    def test_1(self):
        stdout = StringIO()
        with patch.object(sys, 'argv', ['triangulo.py', '1']):
            with contextlib.redirect_stdout(stdout):
                triangulo.main()
            output = stdout.getvalue()
            self.assertEqual(output, result_1)

    def test_9(self):
        stdout = StringIO()
        with patch.object(sys, 'argv', ['triangulo.py', '9']):
            with contextlib.redirect_stdout(stdout):
                triangulo.main()
            output = stdout.getvalue()
            self.assertEqual(output, result_9)


if __name__ == '__main__':
    unittest.main(module=__name__, buffer=True, exit=False)
