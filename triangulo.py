def crear_linea(numero):
    linea = [str(numero)] * numero
# Escribir el numero como si fuese una letra x veces
    return "".join(linea)
def crear_triangulo(numero):
    triangulo = []
    for i in range(1, numero + 1):
        linea = crear_linea(i)
        triangulo.append(linea)
# Lista dinamica
    return "\n".join(triangulo)
# Para escribir después de cada caracter en este caso un pasa a la siguiente linea

def main():
    print("Este programa te hace una pirámide de números repetidos. Para cerrar el programa, introduce 0.")
    while True:
        try:
            number = int(input("Introduce un número (1-9): "))
            if number == 0:
                break
            if number < 1 or number > 9:
                raise ValueError("El número debe estar en el rango de 1 a 9.")
            resultado = crear_triangulo(number)
            print(resultado)
        except ValueError:
             print("Por favor, introduce un número entero entre 1 y 9.")

if __name__ == '__main__':
    main()